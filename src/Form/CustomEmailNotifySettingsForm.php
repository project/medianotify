<?php

namespace Drupal\custom_email_notify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CustomEmailNotifySettingsForm extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return ['custom_email_notify.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'custom_email_notify_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('custom_email_notify.settings');

    $form['allowed_file_types'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Allowed File Types'),
      '#description' => $this->t('Enter the file types separated by commas. For example: pdf,docx'),
      '#default_value' => $config->get('allowed_file_types'),
    ];

    $form['email_recipients'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email Recipients'),
      '#description' => $this->t('Enter the email addresses separated by commas.'),
      '#default_value' => $config->get('email_recipients'),
    ];
    $form['email_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email Subject'),
      '#default_value' => $config->get('email_subject'),
      '#description' => $this->t('Subject for the notification email.'),
    ];

    $form['email_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email Message'),
      '#default_value' => $config->get('email_message'),
      '#description' => $this->t('Message for the notification email. Use [file_url] for the file URL and [current-username] for the user email.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $this->config('custom_email_notify.settings')
      ->set('allowed_file_types', $form_state->getValue('allowed_file_types'))
      ->set('email_recipients', $form_state->getValue('email_recipients'))
      ->set('email_subject', $form_state->getValue('email_subject'))
      ->set('email_message', $form_state->getValue('email_message'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
